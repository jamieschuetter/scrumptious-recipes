from django.urls import path

from recipes.views import (
    RecipeCreateView,
    RecipeUpdateView,
    log_rating,
    RecipeDeleteView,
    RecipeDetailView,
    RecipeListView,
)

urlpatterns = [
    path("", RecipeListView.as_view(), name="recipes_list"),
    path("<int:pk>", RecipeDetailView.as_view(), name="recipe_detail"),
    path("delete/<int:pk>", RecipeDeleteView.as_view(), name="recipe_delete"),
    path("new/", RecipeCreateView.as_view(), name="recipe_new"),
    path("edit/<int:pk>", RecipeUpdateView.as_view(), name="recipe_edit"),
    path("ratings/<int:pk>", log_rating, name="recipe_rating"),
]
